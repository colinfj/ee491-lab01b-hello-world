///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 491 - Software Reverse Engineering
/// Lab 01 - Hello World
///
/// @file hello.c
/// @version 1.0
///
/// @author Colin Jackson <colinfj@hawaii.edu>
/// @brief  Lab 01 - Hello World - EE 491F - Spr 2021
/// @date   12_001_2021
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>

int main() {
	printf("Hello world!\n");

   printf("A fun fact about me:  Avid snowboarder. Haven't gone in a couple years now though, sadly.\n");

   printf("This program [%s] was compiled on [%s] at [%s]\n", __FILE__, __DATE__, __TIME__);

   printf("We are in the [%s] function right now.\n", __FUNCTION__);

#ifdef __INTEL_COMPILER
   printf("The Intel C++ compiler was used for compiling this program\n");
#elif __GNUC__
   printf("The GNU C Compiler version [%s] was used for compiling this program\n", __VERSION__);
#elif _MSC_VER
   printf("The compiler is Microsoft Visual Studio version [%d]\n", _MSC_VER);
#else
   printf("Can not determine the compiler\n");
#endif

   return 0;
}

